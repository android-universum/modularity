##
# *************************************************************************************************
#                                 Copyright 2020 Universum Studios
# *************************************************************************************************
#                  Licensed under the Apache License, Version 2.0 (the "License")
# -------------------------------------------------------------------------------------------------
# You may not use this file except in compliance with the License. You may obtain a copy of the
# License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License
# is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
# or implied.
#
# See the License for the specific language governing permissions and limitations under the License.
# *************************************************************************************************
##
image: universumstudios/android:api-29

### CONFIGURATION ==================================================================================

variables:
  # PRODUCT:
  PRODUCT_ID: "universum.studios.android.template"
  FILE_VERSION_PROPERTIES: "/build_codes/android/${CI_PRODUCT_KEY}/version.properties"
  # GIT:
  GIT_DEPTH: "1"
  # BUILD:
  BUILD_PROPERTIES_PRODUCTION: "-Pandroid.aapt.cruncher.enabled=true -Pfirebase.performancemonitoring.enabled=false"
  BUILD_DIRECTORY_MOBILE: "mobile/build/"
  MAVEN_REPOSITORY: "./.m2/repository"
  MAVEN_OPTS: "-Dmaven.repo.local=${MAVEN_REPOSITORY}"
  GRADLE_HOME: "/cache/.gradle"
  GRADLE_HOME_CACHES: "${GRADLE_HOME}/caches/"
  GRADLE_HOME_WRAPPER: "${GRADLE_HOME}/wrapper/"

before_script:
  # Grant permissions for .sh files used by this CI script.
  - ./ci/permissions.sh
  # Perform checks ensuring that we have everything for the CI build.
  - ./ci/checks.sh
  # Download keystore with signing keys used to sign APK.
  - ./ci/download-keystore.sh
  # Export environment variables.
  - export ANDROID_UNIVERSUM_KEYSTORE="/keystore"
  - export GRADLE_USER_HOME="${GRADLE_HOME}"

cache:
  paths:
    - ${MAVEN_REPOSITORY}
    - ${GRADLE_HOME_CACHES}
    - ${GRADLE_HOME_WRAPPER}

stages:
  - Build
  - Test
  - Accept
  - Deploy

### JOBS ===========================================================================================

## BUILD -------------------------------------------------------------------------------------------

# Job which builds code base for a particular FEATURE/BUG-FIX/PATCH.
build:development:
  stage: Build
  cache:
    key: "pipeline-${CI_PIPELINE_ID}"
    paths:
      - ${BUILD_DIRECTORY_MOBILE}
    policy: push
  script:
    - ./gradlew :mobile:buildDevelopment
  only:
    - /^feature.*$/
    - /^bugfix.*$/
    - /^patch.*$/

# Job which builds code base for the STAGING environment.
build:staging:
  stage: Build
  cache:
    key: "pipeline-${CI_PIPELINE_ID}"
    paths:
      - ${BUILD_DIRECTORY_MOBILE}
    policy: push
  script:
    - ./ci/increment-version-code.sh ${FILE_VERSION_PROPERTIES}
    - ./gradlew :mobile:buildStaging ${BUILD_PROPERTIES_PRODUCTION}
  only:
    - develop
    - /^release.*$/
    - /^hotfix.*$/

# Job which builds code base for the PRODUCTION environment.
build:production:
  stage: Build
  cache:
    key: "pipeline-${CI_PIPELINE_ID}"
    paths:
      - ${BUILD_DIRECTORY_MOBILE}
    policy: push
  script:
    - ./ci/increment-version-code.sh ${FILE_VERSION_PROPERTIES}
    - ./gradlew :mobile:buildProduction ${BUILD_PROPERTIES_PRODUCTION}
  artifacts:
    paths:
      - mobile/deployment/production/artifact
    expire_in: 1 week
  only:
    - master

## TEST --------------------------------------------------------------------------------------------

# Job which executes tests against code base for a particular FEATURE/BUG-FIX/PATCH.
test:development:
  stage: Test
  cache:
    key: "pipeline-${CI_PIPELINE_ID}"
    paths:
      - ${BUILD_DIRECTORY_MOBILE}
    policy: pull
  script:
    - ./gradlew :mobile:testDevelopmentWithCoverage
  artifacts:
    reports:
      junit: mobile/build/test-results/testDevelopmentDebugUnitTest/TEST-*.xml
  only:
    - /^feature.*$/
    - /^bugfix.*$/
    - /^patch.*$/

# Job which executes tests against code base for the STATING environment.
test:staging:
  stage: Test
  cache:
    key: "pipeline-${CI_PIPELINE_ID}"
    paths:
      - ${BUILD_DIRECTORY_MOBILE}
    policy: pull
  script:
    - ./gradlew :mobile:testStagingWithCoverage
  artifacts:
    reports:
      junit: mobile/build/test-results/testStagingReleaseUnitTest/TEST-*.xml
  only:
    - develop
    - /^release.*$/
    - /^hotfix.*$/

# Job which executes tests against code base for the PRODUCTION environment.
test:production:
  stage: Test
  cache:
    key: "pipeline-${CI_PIPELINE_ID}"
    paths:
      - ${BUILD_DIRECTORY_MOBILE}
    policy: pull
  script:
    - ./gradlew :mobile:testProductionWithCoverage
  artifacts:
    reports:
      junit: mobile/build/test-results/testProductionReleaseUnitTest/TEST-*.xml
  only:
    - master

# Job which executes tests against code base for the TEST environment.
test:test:
  stage: Test
  script:
    - ./gradlew :mobile:testProductionWithCoverage
  only:
    - test

## ACCEPT ------------------------------------------------------------------------------------------

# Job which executes Acceptance tests against code base for the STAGING environment.
accept:staging:
  stage: Accept
  cache:
    key: "pipeline-${CI_PIPELINE_ID}"
    paths:
      - ${BUILD_DIRECTORY_MOBILE}
    policy: pull
  script:
    # Build artifacts:
    - ./gradlew :mobile:assembleAcceptanceArtifacts
    # Authenticate with Google Cloud:
    # todo: update project name ...
    - gcloud config set project mobile-app
    - gcloud auth activate-service-account ${CI_GCLOUD_SERVICE_ACCOUNT} --key-file ${CI_GCLOUD_SERVICE_ACCOUNT_KEY}
    # Execute tests via Firebase:
    - gcloud firebase test android run
        --type instrumentation
        --app mobile/deployment/acceptance/artifact/surglogs.apk
        --test mobile/deployment/acceptance/artifact/surglogs-test.apk
        --device model=hwALE-H,version=21,locale=en_US,orientation=portrait
        --device model=shamu,version=22,locale=en_US,orientation=portrait
        --device model=Nexus5,version=23,locale=en_US,orientation=portrait
        --device model=Nexus6,version=24,locale=en_US,orientation=portrait
        --device model=Nexus9,version=25,locale=en_US,orientation=portrait
        --device model=Nexus5X,version=26,locale=en_US,orientation=portrait
        --device model=Nexus6P,version=27,locale=en_US,orientation=portrait
        --device model=Pixel2,version=28,locale=en_US,orientation=portrait
        --device model=blueline,version=Q-beta-3,locale=en_US,orientation=portrait
        --environment-variables coverage=true,coverageFile="/sdcard/coverage.ec"
        --timeout 10m
    # Use '--directories-to-pull /sdcard' before '--timeout 10m' to pull coverage file from device to Google Cloud Storage.
  only:
    - develop
    - /^release.*$/
    - /^hotfix.*$/
  when: manual

## DEPLOY ------------------------------------------------------------------------------------------

# Job which uploads release artifacts into the STATING environment.
deploy:staging:
  stage: Deploy
  environment:
    name: Staging
    url: https://fabric.io/surglogs/android/apps/${PRODUCT_ID}.staging/beta/releases/latest
  cache:
    key: "pipeline-${CI_PIPELINE_ID}"
    paths:
      - ${BUILD_DIRECTORY_MOBILE}
    policy: pull
  script:
    - ./gradlew :mobile:deployStaging
  only:
    - develop
    - /^release.*$/
    - /^hotfix.*$/
  when: manual

# Job which uploads release artifacts into the PRODUCTION environment.
deploy:production:
  stage: Deploy
  environment:
    name: Production
    url: https://play.google.com/store/apps/details?id=${PRODUCT_ID}
  cache:
    key: "pipeline-${CI_PIPELINE_ID}"
    paths:
      - ${BUILD_DIRECTORY_MOBILE}
    policy: pull
  script:
    - ./gradlew :mobile:deployProduction
  only:
    - master
  when: manual