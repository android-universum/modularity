### Overview ###

This is a request to merge _Fix_ **[SUR-{FIX_ID}](https://universum-studios.atlassian.net/browse/MODULARITY-{FIX_ID})**
into the **target** branch.

### Changes ###
> Below are listed changes that will be **incorporated** into code base of the target branch when **accepted**.

- **{SECTION}:**
- {LIST CHANGES HERE}

/label ~bug
/assign @martin.albedinsky