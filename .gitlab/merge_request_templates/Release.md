### Overview ###

This is a request to merge **quality** and **stability improvements** of _Release_ **{RELEASE_NAME}**
into the **master** branch.

/label ~release
/assign @martin.albedinsky