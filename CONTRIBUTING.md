Contributing
============

## Code Style ##

## Architecture ##

## Tests ##

### Unit ###

### Integration ###

### Acceptance ###

## Continuous Integration ##

Always ensure that **Gradle** tasks (_build, test, deploy, ..._) that are executed in **CI jobs** as part of a particular **CI pipeline** for every supported environment
always succeed after following changes have been made to overall project's configuration:

- **Gradle** update (version, configuration, tasks, ...),
- **Android Gradle Plugin** update (version),
- **dependencies** update (versions, structure),
- **modules** update (code base)

> Note that list above does not exhaust all the possible changes which may be made to the project's CI related configuration.