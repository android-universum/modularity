/*
 * *************************************************************************************************
 *                                 Copyright 2020 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.modularity.account.authentication.interaction

import universum.studios.android.arkhitekton.interaction.usecase.UseCase
import universum.studios.android.arkhitekton.util.Value
import universum.studios.modularity.account.authentication.Credentials

/**
 * @author Martin Albedinsky
 */
interface SignInUseCase : UseCase<SignInUseCase.Request, Value.Empty> {

    data class Request(val credentials: Credentials) : universum.studios.android.arkhitekton.interaction.Request
}