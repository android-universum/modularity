/*
 * *************************************************************************************************
 *                                 Copyright 2020 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.modularity.account.authentication.interaction

import universum.studios.android.arkhitekton.interaction.Response
import universum.studios.android.arkhitekton.interaction.usecase.BaseUseCase
import universum.studios.android.arkhitekton.util.Value
import universum.studios.modularity.account.authentication.interaction.SignInUseCase.Request

/**
 * @author Martin Albedinsky
 */
internal class SignInUseCaseImpl(builder: Builder) : BaseUseCase<SignInUseCase.Request, Value.Empty>(builder), SignInUseCase {

    override fun onExecute(request: Request): Response<Value.Empty> {
        return createSuccess(request, Value.EMPTY)
    }

    class Builder : BaseUseCase.BaseBuilder<Builder>() {

        override val self = this
        override fun build() = SignInUseCaseImpl(this)
    }
}