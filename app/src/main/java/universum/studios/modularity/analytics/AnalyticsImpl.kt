/*
 * *************************************************************************************************
 *                                 Copyright 2020 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.modularity.analytics

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.core.CrashlyticsCore
import universum.studios.android.analytics.AnalyticsFacade
import universum.studios.android.analytics.ConsoleDestination
import universum.studios.android.analytics.FabricDestination
import universum.studios.android.analytics.screen.ScreenAnalytics
import universum.studios.modularity.util.Logging

/**
 * @author Martin Albedinsky
 */
class AnalyticsImpl private constructor(builder: Builder) : AnalyticsFacade(builder) {

    companion object {

        fun buildForEnvironment(application: Application, @AnalyticsPolices.Environment environment: Int): AnalyticsImpl {
            return when(environment) {
                AnalyticsPolices.ENVIRONMENT_PRODUCTION -> {
                    val answers = Answers()
                    val crashlytics = Crashlytics.Builder()
                            .core(CrashlyticsCore.Builder().disabled(AnalyticsPolices.Fabric.DISABLED).build())
                            .answers(answers)
                            .build()
                    io.fabric.sdk.android.Fabric.with(application, crashlytics, answers)
                    val analytics = Builder()
                            .addDestination(FabricDestination.create(crashlytics))
                            .addDestination(ConsoleDestination.create(Logging.logger))
                            .build()
                    ScreenAnalytics.Builder().analytics(analytics).destinations(AnalyticsPolices.DESTINATION_SCREEN_EVENTS).build().install(application)
                    analytics
                }
                AnalyticsPolices.ENVIRONMENT_TEST -> Builder().addDestination(ConsoleDestination.create(Logging.logger)).build()
                else -> Builder().build()
            }
        }
    }

    class Builder : AnalyticsFacade.FacadeBuilder<Builder>() {

        override val self = this
        override fun build() = AnalyticsImpl(this)
    }
}