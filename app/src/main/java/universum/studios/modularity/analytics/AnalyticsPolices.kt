/*
 * *************************************************************************************************
 *                                 Copyright 2020 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.modularity.analytics

import androidx.annotation.IntDef
import universum.studios.android.analytics.FabricDestination
import universum.studios.modularity.BuildConfig

/**
 * @author Martin Albedinsky
 */
class AnalyticsPolices private constructor() {

    companion object {

        const val ENVIRONMENT_PRODUCTION = 0
        const val ENVIRONMENT_TEST = 1

        const val DESTINATION_EVENTS = FabricDestination.NAME
        const val DESTINATION_SCREEN_EVENTS = FabricDestination.NAME
        const val DESTINATION_FAILURES = FabricDestination.NAME
    }

    @IntDef(ENVIRONMENT_PRODUCTION, ENVIRONMENT_TEST)
    @Retention(AnnotationRetention.SOURCE)
    annotation class Environment

    object Fabric {

        internal const val DISABLED = BuildConfig.BUILD_TYPE == "debug"
    }
}