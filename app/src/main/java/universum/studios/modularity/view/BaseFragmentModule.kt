/*
 * =================================================================================================
 *                                    Copyright (C) 2017 SurgLogs
 * =================================================================================================
 */
package universum.studios.modularity.view

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import universum.studios.android.arkhitekton.control.Controller

/**
 * @author Martin Albedinsky
 */
abstract class BaseFragmentModule {

    companion object {

        fun <T : Controller.Holder<*>> provideControllerHolder(fragment: Fragment, holderClass: Class<T>): T = provideViewModelProvider(fragment).get(holderClass)
        fun <T : ViewModel> provideViewModel(fragment: Fragment, modelClass: Class<T>) = provideViewModelProvider(fragment).get(modelClass)
        fun provideViewModelProvider(fragment: Fragment) = ViewModelProviders.of(fragment)
        fun provideViewModelProvider(fragment: Fragment, factory: ViewModelProvider.Factory) = ViewModelProviders.of(fragment, factory)
    }
}