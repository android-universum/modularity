#!/usr/bin/env bash

function checkThatFileExists() {
  [[ -f ${2} ]] && echo "Check: File '${1}' found on the CI server." || echo "Check: File '${1}' not found on the CI server at path '${2}'!"
}

checkThatFileExists "version.properties" "${FILE_VERSION_PROPERTIES}"