// Defines dependencies used across the project.
def versions = [
        kotlin     : '1.3.70',
        androidx   : [
                lifecycle: '2.2.0',
                room     : '2.2.4',
                test     : [espresso: '3.2.0']
        ],
        butterknife: '10.2.1',
        google     : [dagger: '2.26'],
        permissions: [dispatcher: '4.5.0'],
        squareup   : [
                okhttp  : '4.4.0',
                retrofit: '2.7.2'
        ],
        test       : [
                cucumber   : '4.4.0',
                junit      : '4.13',
                mockito    : '3.3.0',
                robolectric: '4.3.1'
        ]
]

def kotlin = [
        stdlib    : "org.jetbrains.kotlin:kotlin-stdlib:${versions.kotlin}",
        stdlibJdk7: "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${versions.kotlin}",
        stdlibJdk8: "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${versions.kotlin}"
]

def androidx = [
        annotation      : [annotation: "androidx.annotation:annotation:1.1.0"],
        appcompat       : [appcompat: "androidx.appcompat:appcompat:1.1.0"],
        cardview        : [cardview: "androidx.cardview:cardview:1.0.0"],
        constraintlayout: [constraintlayout: "androidx.constraintlayout:constraintlayout:1.1.3"],
        core            : [
                core   : "androidx.core:core:1.2.0",
                testing: "androidx.core:core-testing:1.2.0"
        ],
        drawerlayout    : [drawerlayout: "androidx.drawerlayout:drawerlayout:1.0.0"],
        fragment        : [
                fragment: "androidx.fragment:fragment:1.2.2",
                testing : "androidx.fragment:fragment-testing:1.2.2"
        ],
        legacy          : [
                coreUi    : "androidx.legacy:legacy-support-core-ui:1.0.0",
                coreUtils : "androidx.legacy:legacy-support-core-utils:1.0.0",
                supportV4 : "androidx.legacy:legacy-support-v4:1.0.0",
                supportV13: "androidx.legacy:legacy-support-v13:1.0.0"
        ],
        lifecycle       : [
                runtime        : "androidx.lifecycle:lifecycle-runtime:${versions.androidx.lifecycle}",
                extensions     : "androidx.lifecycle:lifecycle-extensions:${versions.androidx.lifecycle}",
                processor      : "androidx.lifecycle:lifecycle-compiler:${versions.androidx.lifecycle}",
                reactiveStreams: "androidx.lifecycle:lifecycle-reactivestreams:${versions.androidx.lifecycle}",
        ],
        multidex        : [multidex: 'androidx.multidex:multidex:2.0.1'],
        preference      : [preference: "androidx.preference:preference:1.1.0"],
        recyclerview    : [recyclerview: "androidx.recyclerview:recyclerview:1.1.0"],
        room            : [
                runtime  : "androidx.room:room-runtime:${versions.androidx.room}",
                processor: "androidx.room:room-compiler:${versions.androidx.room}",
                rxJava   : "androidx.room:room-rxjava2:${versions.androidx.room}",
                testing  : "androidx.room:room-testing:${versions.androidx.room}"
        ],
        test            : [
                core    : "androidx.test:core:1.2.0",
                espresso: [
                        contrib: "androidx.test.espresso:espresso-contrib:${versions.androidx.test.espresso}",
                        core   : "androidx.test.espresso:espresso-core:${versions.androidx.test.espresso}",
                        intents: "androidx.test.espresso:espresso-intents:${versions.androidx.test.espresso}"
                ],
                ext     : [junit: "androidx.test.ext:junit:1.1.1"],
                rules   : "androidx.test:rules:1.2.0",
                runner  : "androidx.test:runner:1.2.0"
        ],
        vectordrawable  : [vectordrawable: "androidx.vectordrawable:vectordrawable:1.1.0"]
]

def google = [
        android : [material: [material: "com.google.android.material:material:1.1.0"]],
        dagger  : [
                dagger          : "com.google.dagger:dagger:${versions.google.dagger}",
                processor       : "com.google.dagger:dagger-compiler:${versions.google.dagger}",
                android         : "com.google.dagger:dagger-android:${versions.google.dagger}",
                androidSupport  : "com.google.dagger:dagger-android-support:${versions.google.dagger}",
                androidProcessor: "com.google.dagger:dagger-android-processor:${versions.google.dagger}"
        ],
        firebase: [
                analytics  : 'com.google.firebase:firebase-analytics:17.2.1',
                crashlytics: 'com.crashlytics.sdk.android:crashlytics:2.10.1@aar',
                messaging  : 'com.google.firebase:firebase-messaging:20.0.1',
                performance: 'com.google.firebase:firebase-perf:19.0.2'
        ],
        truth   : [truth: "com.google.truth:truth:1.0.1"],
        other   : [gson: 'com.google.code.gson:gson:2.8.5']
]

def test = [
        assertj    : "org.assertj:assertj-core:3.15.0",
        cucumber   : [
                android      : "io.cucumber:cucumber-android:${versions.test.cucumber}",
                core         : "io.cucumber:cucumber-core:${versions.test.cucumber}",
                java         : "io.cucumber:cucumber-java:${versions.test.cucumber}",
                junit        : "io.cucumber:cucumber-junit:${versions.test.cucumber}",
                picocontainer: "io.cucumber:cucumber-picocontainer:${versions.test.cucumber}"
        ],
        junit      : "junit:junit:${versions.test.junit}",
        mockito    : [
                core   : "org.mockito:mockito-core:${versions.test.mockito}",
                android: "org.mockito:mockito-android:${versions.test.mockito}"
        ],
        mock       : [webServer: "com.squareup.okhttp3:mockwebserver:${versions.squareup.okhttp}"],
        robolectric: [
                robolectric     : "org.robolectric:robolectric:${versions.test.robolectric}",
                shadowsMultidex : "org.robolectric:shadows-multidex:${versions.test.robolectric}",
                shadowsSupportV4: "org.robolectric:shadows-supportv4:${versions.test.robolectric}"
        ]
]

def rx = [
        android   : 'io.reactivex.rxjava3:rxandroid:3.0.0',
        java      : 'io.reactivex.rxjava3:rxjava:3.0.0',
        javaBridge: 'com.github.akarnokd:rxjava3-bridge:3.0.0'
]

def universum = [studios: [
        analytics     : 'universum.studios.android:analytics:1.3.1@aar',
        arkhitekton   : 'universum.studios.android:arkhitekton:2.0.0@aar',
        crypto        : 'universum.studios.android:crypto:0.3.1@aar',
        dialogs       : 'universum.studios.android:dialogs:0.11.0@aar',
        font          : [core: 'universum.studios.android:font-core:1.2.1@aar'],
        fragments     : 'universum.studios.android:fragments:1.5.0@aar',
        graphics      : [colorUtil: 'universum.studios.android:graphics-color-util:1.1.1@aar'],
        intents       : 'universum.studios.android:intents:1.2.1@aar',
        logger        : 'universum.studios.android:logger:1.1.2@aar',
        officium      : 'universum.studios.android:officium:2.0.3@aar',
        pagerAdapters : 'universum.studios.android:pager-adapters:2.2.1@aar',
        preferences   : [core: 'universum.studios.android:preferences-core:2.2.1@aar'],
        recycler      : 'universum.studios.android:recycler:1.2.2@aar',
        settings      : 'universum.studios.android:settings:1.1.2@aar',
        testing       : 'universum.studios.android:testing:1.1.2@aar',
        transitions   : 'universum.studios.android:transitions:1.4.1@aar',
        ui            : 'universum.studios.android:ui:0.10.6@aar',
        universi      : 'universum.studios.android:universi:1.3.0@aar',
        utils         : 'universum.studios.android:utils:2.2.1@aar',
        versioning    : 'universum.studios.android:versioning:1.1.3@aar',
        widgetAdapters: 'universum.studios.android:widget-adapters:2.1.2@aar'
]]

def other = [
        butterknife: [
                butterknife: "com.jakewharton:butterknife:${versions.butterknife}",
                processor  : "com.jakewharton:butterknife-compiler:${versions.butterknife}"
        ],
        commonsIo  : 'commons-io:commons-io:2.6',
        jetbrains  : [annotations: 'org.jetbrains:annotations:13.0'],
        jodaTime   : 'joda-time:joda-time:2.10.5',
        permissions: [
                dispatcher         : "org.permissionsdispatcher:permissionsdispatcher:${versions.permissions.dispatcher}",
                dispatcherProcessor: "org.permissionsdispatcher:permissionsdispatcher-processor:${versions.permissions.dispatcher}"
        ],
        squareup   : [
                okio                      : 'com.squareup.okio:okio:2.4.3',
                okhttp                    : "com.squareup.okhttp3:okhttp:${versions.squareup.okhttp}",
                okhttpUrlConnection       : "com.squareup.okhttp3:okhttp-urlconnection:${versions.squareup.okhttp}",
                okhttpLoggingInterceptor  : "com.squareup.okhttp3:logging-interceptor:${versions.squareup.okhttp}",
                retrofit                  : "com.squareup.retrofit2:retrofit:${versions.squareup.retrofit}",
                retrofitAdapterRxJava     : "com.squareup.retrofit2:adapter-rxjava2:${versions.squareup.retrofit}",
                retrofitConverterGson     : "com.squareup.retrofit2:converter-gson:${versions.squareup.retrofit}",
                retrofitConverterSimpleXml: "com.squareup.retrofit2:converter-simplexml:${versions.squareup.retrofit}"
        ]
]

def debug = [
        database  : 'com.amitshekhar.android:debug-db:1.0.1',
        facebook  : [
                stetho      : 'com.facebook.stetho:stetho:1.5.1',
                stethoOkHttp: 'com.facebook.stetho:stetho-okhttp:1.5.1'
        ],
        leakcanary: [android: 'com.squareup.leakcanary:leakcanary-android:2.2'],
        sqlscout  : [
                server    : 'com.idescout.sql:sqlscout-server:4.1',
                serverNoop: 'com.idescout.sql:sqlscout-server-noop:4.1'
        ]
]

ext.versions = versions
ext.deps = [
        "androidx" : androidx,
        "debug"    : debug,
        "google"   : google,
        "kotlin"   : kotlin,
        "other"    : other,
        "rx"       : rx,
        "test"     : test,
        "universum": universum
]